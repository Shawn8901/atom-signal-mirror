FROM ubuntu:20.04
WORKDIR /app/atom-signal-mirror
ENV VERSION=0.9.2
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && apt-get install wget openjdk-11-jre git python3 python3-pip -y && apt-get clean
RUN wget https://github.com/AsamK/signal-cli/releases/download/v"${VERSION}"/signal-cli-"${VERSION}".tar.gz
RUN tar xfvz signal-cli-"${VERSION}".tar.gz -C /opt
RUN ln -sf /opt/signal-cli-"${VERSION}"/bin/signal-cli /usr/local/bin/
COPY atom-signal-mirror.py /app/atom-signal-mirror
COPY requirements.txt /app/atom-signal-mirror
RUN pip3 install -r requirements.txt
RUN whereis python3
RUN python3 atom-signal-mirror.py --signal-cli-path signal-cli -h
ENTRYPOINT ["/usr/bin/python3", "atom-signal-mirror.py", "--signal-cli-path", "signal-cli", "--temp-path", "/app/atom-signal-mirror/temp"]
